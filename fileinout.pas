unit fileInOut;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LazFileUtils, LazUTF8, fileutil;

function getFile(Path: string) : TMemoryStream; 
procedure createBackupFile(Path: string);

implementation

function getFile(Path: string) : TMemoryStream;
begin
  getFile := TMemoryStream.Create;

  getFile.LoadFromFile(UTF8toSys(Path));
end;

procedure createBackupFile(Path: string);
begin
  if not FileExists(Path + '.bak') then
     CopyFile(Path, Path + '.bak');
end;

end.

