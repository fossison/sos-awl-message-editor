unit MessageEditorForm;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, Menus, Grids,
  LCLIntf, LCLType, ExtCtrls, StdCtrls, LazLogger, strutils,
  awlmessage;

function ReplaceLastOccurrence(str: String; find: String; replace: String): String;

type
  TFileStatus = (none, opened, changed, saved);

  { TMessageEditorForm }

  TMessageEditorForm = class(TForm)
    ApplyChangesButton: TButton;
    FindDialog: TFindDialog;
    UtilitiesMenuTrim: TMenuItem;
    MessageMemo: TMemo;
    MessageTitleEdit: TLabeledEdit;
    MainMenu: TMainMenu;
    FileMenu: TMenuItem;
    AboutMenu: TMenuItem;
    FileMenuOpen: TMenuItem;
    FileMenuSave: TMenuItem;
    EditMenu: TMenuItem;
    EditMenuFind: TMenuItem;
    EditMenuFindReplace: TMenuItem;
    OpenDialog: TOpenDialog;
    ReplaceDialog: TReplaceDialog;
    SaveDialog: TSaveDialog;
    MsgGrid: TStringGrid;
    UtilitiesMenu: TMenuItem; 
    procedure ApplyChangesButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure MsgGridPrepareCanvas(Sender: TObject; aCol, aRow: Integer;
      aState: TGridDrawState);
    procedure MsgGridSelection(Sender: TObject; aCol, aRow: Integer);
    procedure FindDialogFind(Sender: TObject);
    procedure ReplaceDialogReplace(Sender: TObject);
    procedure ResetForm;
    procedure PopulateMsgGrid;                   
    procedure UpdateMesFromGrid;
    procedure EditMenuFindClick(Sender: TObject);
    procedure EditMenuFindReplaceClick(Sender: TObject);
    procedure FileMenuOpenClick(Sender: TObject);
    procedure FileMenuSaveClick(Sender: TObject);

    function CalcCellHeight(AGrid: TStringGrid; ACol, ARow: Integer): Integer;
    function CalcRowHeight(AGrid: TStringGrid; ARow: Integer): Integer;
    procedure AutoRowHeights(AGrid: TStringGrid);
    procedure UtilitiesMenuTrimClick(Sender: TObject);
  private

  public

  end;

var
  MessageEditor: TMessageEditorForm;
  fileSaved: Boolean = false;
  fileStatus: TFileStatus = none;

implementation

{$R *.lfm}


function ReplaceLastOccurrence(str: String; find: String; replace: String): String;
var
  x2: String;
  p: Integer;
begin
  x2 := str;
  p := RPos(find, str);
  if (p > 0) then
  begin
    Delete(x2, p, Length(find));
    Insert(replace, x2, p);
  end;

  ReplaceLastOccurrence := x2;
end;

procedure openFile(p: String);
begin
  mes := TMes.Create(p);
  mes.LoadFromFile;
end;

procedure TMessageEditorForm.PopulateMsgGrid; 
var
  I: Integer = 0;
begin
  if mes.GameType = sosawl then
    begin
      MsgGrid.RowCount := Length(Mes.MsgCollection) + 1;

      for I := 0 to Length(Mes.MsgCollection) - 1 do
        begin
          MsgGrid.Cells[0, I + 1] := IntToStr(I + 1);
          MsgGrid.Cells[1, I + 1] := Mes.MsgCollection[I].mTitle;
          MsgGrid.Cells[2, I + 1] := Mes.MsgCollection[I].mText;
          MsgGrid.Cells[3, I + 1] := '0';
        end;
    end
  else
    begin
      { TODO: Re-enabled GameCube and PS2 Support }
    end;

  AutoRowHeights(MsgGrid);
  for I := 0 to MsgGrid.ColCount do
    MsgGrid.AutoSizeColumn(I);
end;

procedure TMessageEditorForm.UpdateMesFromGrid;
var
  I: Integer;
begin
    // Resize the array of our message records
  setLength(mes.MsgCollection,0);
  setLength(mes.MsgCollection, MsgGrid.RowCount - 1);

  for I := 1 to MsgGrid.RowCount - 1 do
    begin
      with mes.MsgCollection[I - 1] do
      begin
        if Trim(MsgGrid.Cells[0, I]) <> '' then
          begin
            mTitle := Trim(MsgGrid.Cells[1, I]) + #0;
            mText := Trim(StringReplace(MsgGrid.Cells[2, I], #13, '', [rfReplaceAll])) + #0;
          end;
      end;
    end;
end;

  { TMessageEditorForm }

procedure TMessageEditorForm.FormCreate(Sender: TObject);
begin

end;

procedure TMessageEditorForm.MsgGridPrepareCanvas(Sender: TObject; aCol,
  aRow: Integer; aState: TGridDrawState);
begin
  if not (Sender is TStringGrid) then Exit;

  with Sender as TStringGrid do
  begin
    if Cells[4, aRow] = '1' then
      Canvas.Brush.Color := clSilver;
  end;
end;

procedure TMessageEditorForm.MsgGridSelection(Sender: TObject; aCol,
  aRow: Integer);
begin
  MessageTitleEdit.Text := (sender as TStringGrid).Cells[1, aRow];
  MessageMemo.Text := (sender as TStringGrid).Cells[2, aRow];
end;


procedure TMessageEditorForm.ResetForm;
begin   
  MsgGrid.RowCount := 1;
  //TitleEdit.Text := '';
  //MsgContent.Text := '';
end;

procedure TMessageEditorForm.FileMenuOpenClick(Sender: TObject);
begin
  if OpenDialog.Execute then
    begin
      openFile(OpenDialog.FileName);
      ResetForm;
      PopulateMsgGrid;
      //FileName.Caption := ExtractFileName(OpenDialog.FileName);
    end;
end;

procedure TMessageEditorForm.FileMenuSaveClick(Sender: TObject);
begin
  if SaveDialog.Execute then
    begin
      UpdateMesFromGrid;
      mes.writeFile(SaveDialog.FileName);
    end;
end;

procedure TMessageEditorForm.EditMenuFindClick(Sender: TObject);
begin
  with FindDialog do
  begin
    Execute;
  end;
end;

procedure TMessageEditorForm.FindDialogFind(Sender: TObject);
var
  I: Integer;
  FindStr: String;
  SearchStr: String;
begin
  with Sender as TFindDialog do
  begin
    FindStr := FindText;
    if not (frMatchCase in Options) then
      FindStr := LowerCase(FindStr);

    for I := 1 to MsgGrid.RowCount - 1 do
    begin
      SearchStr := MsgGrid.Cells[2, I];
      if not (frMatchCase in Options) then
        SearchStr := LowerCase(SearchStr);
      if PosEx(FindStr, SearchStr) > 0 then
        MsgGrid.Cells[4, I] := IntToStr(1)
      else
        MsgGrid.Cells[4, I] := IntToStr(0)
    end;
  end;
  Refresh;
end;

procedure TMessageEditorForm.EditMenuFindReplaceClick(Sender: TObject);
begin
  with ReplaceDialog do
  begin
    Execute;
  end;
end;

procedure TMessageEditorForm.ReplaceDialogReplace(Sender: TObject);
var
  I: Integer;
  FindStr: String;
  SearchStr: String;
begin
  with Sender as TReplaceDialog do
  begin
    FindStr := FindText;
    if not (frMatchCase in Options) then
      FindStr := LowerCase(FindStr);


    for I := 1 to MsgGrid.RowCount - 1 do
    begin
      if not (frMatchCase in Options) then
        MsgGrid.Cells[2, I] := StringReplace(MsgGrid.Cells[2, I], FindText, ReplaceText, [rfReplaceAll, rfIgnoreCase])
      else
        MsgGrid.Cells[2, I] := StringReplace(MsgGrid.Cells[2, I], FindText, ReplaceText, [rfReplaceAll]);

      SearchStr := MsgGrid.Cells[2, I];
      if not (frMatchCase in Options) then
        SearchStr := LowerCase(SearchStr);
      if PosEx(FindStr, SearchStr) > 0 then
        begin
          MsgGrid.Cells[3, I] := '1';
          MsgGrid.Cells[4, I] := '1';
        end
      else
        MsgGrid.Cells[4, I] := IntToStr(0)
    end;
  end;
  Refresh;
end;

procedure TMessageEditorForm.ApplyChangesButtonClick(Sender: TObject);
begin
  MsgGrid.Cells[1, MsgGrid.Row] := MessageTitleEdit.Text;
  MsgGrid.Cells[2, MsgGrid.Row] := StringReplace(MessageMemo.Text, #13, '', [rfReplaceAll]);
  MsgGrid.Cells[3, MsgGrid.Row] := '1';
end;

function TMessageEditorForm.CalcCellHeight(AGrid: TStringGrid; ACol, ARow: Integer): Integer;
var
  R: TRect;
  s: String;
begin
  R := AGrid.CellRect(ACol, ARow);
  R.Bottom := R.Top;
  s := AGrid.Cells[ACol, ARow];
  DrawText(AGrid.Canvas.Handle, PChar(s), Length(s), R, DT_CALCRECT or DT_NOPREFIX); //  DT_WORDBREAK or
  Result := R.Bottom - R.Top;
end;

// This calculates the maximum required cell height of a specific row
function TMessageEditorForm.CalcRowHeight(AGrid: TStringGrid; ARow: Integer): Integer;
var
  col: Integer;
  h, hmax: Integer;
begin
  hmax := AGrid.DefaultRowHeight;
  for col := 0 to AGrid.ColCount-1 do
  begin
    h := CalcCellHeight(AGrid, col, ARow);
    if h > hmax then hmax := h;
  end;
  Result := hmax;
end;

// This is the final procedure. It calls CalcRowHeight for all rows of the grid.
procedure TMessageEditorForm.AutoRowHeights(AGrid: TStringGrid);
var
  row: Integer;
begin
  for row := 0 to AGrid.RowCount-1 do
    AGrid.RowHeights[row] := CalcRowHeight(AGrid, row) + varCellPadding;
end;

procedure TMessageEditorForm.UtilitiesMenuTrimClick(Sender: TObject);
var
  I: Integer;
  tmpInStr: String;
  tmpOutStr: String;
  currChar: Char;
  currVisPos: Integer = 0;
  stopTrimAtPos: Integer = 0;
  countChar: Boolean;
  numSpaces: Integer = 0;
begin
  for I := 1 to MsgGrid.RowCount - 1 do
  begin
    tmpInStr := MsgGrid.Cells[2, I];

    countChar := true;
    tmpOutStr := '';
    currVisPos := 0;

    // If the message has a selector flag in it, we don't want to adjust after that
    stopTrimAtPos := PosEx('<sel', tmpInStr) - 2;
    // If we went negative, reset it to 0. 0 means that it wasn't found.
    if stopTrimAtPos < 0 then stopTrimAtPos := 0;

    for currChar in tmpInStr do                   // iterate every character in the string
    begin
      if
        (stopTrimAtPos > 0)                       // if we have a stopping point
        and (Length(tmpOutStr) >= stopTrimAtPos)  // and we're past the stopping point
      then
      begin
        tmpOutStr := tmpOutStr + currChar;        // copy the character as-is
        Continue;                                 // copy until the end (ends current for iteration)
      end;

      // Otherwise, let's reformat these line breaks
      if currChar = #10 then
      begin
        tmpOutStr := tmpOutStr + ' ';              // replace the existing line break with a space
        Inc(numSpaces);                            // increase the number of spaces we have
      end
      else
        tmpOutStr := tmpOutStr + currChar;

      if (stopTrimAtPos > 0) and (Length(tmpOutStr) >= stopTrimAtPos) then
        Continue;                                  // start copying to the end if we're at the selector

      if currChar in ['<', '>'] then               // don't count characters within flags
        countChar := not countChar;

      if countChar then                            // if we count it, increase our "visible position"
        Inc(currVisPos);

       // if we're at/past visible position 52, replace the most recent space with a newline
      if currVisPos >= 52 then
      begin
        tmpOutStr := ReplaceLastOccurrence(tmpOutStr, ' ', #10);
        currVisPos := 0;
      end;
    end;

    MsgGrid.Cells[2, I] := tmpOutStr;
    MsgGrid.Cells[3, I] := '1';
  end;
end;


end.

