unit sosNpcScheduleFile;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, LazLogger;

type
  TNPCScheduleActivity = class
    start_minute: LongWord;
    end_minute: LongWord;
    task_id: LongWord;
    constructor Create;
    procedure SetTaskId(val: LongWord);
    procedure SetTime(s: LongWord; e: LongWord);
  end;

  TNPCScheduleActivityList = class
  private
    raw_SchedFlags: LongWord;
    raw_YearFlags: LongWord;
  public                  
    list_size: LongWord;
    YearFlags: array[0..7] of Boolean;
    SchedFlags: array[0..23] of Boolean;
    ScheduleActivities: array of TNPCScheduleActivity;
    constructor Create;
    procedure SetYearFlags(flags_to_set: LongWord);
    procedure SetSchedFlags(flags_to_set: LongWord);
    procedure AddActivity(s, e, t: LongWord);
    procedure ResetActivities;
  end;

  TNPCSchedule = class
    schedule_size: LongWord;
    ScheduleLists: array of TNPCScheduleActivityList;
    constructor Create(size_of_schedule: Integer);
  end;

  TNPCScheduleFile = class
    file_size: LongWord;
    Schedules: array of TNPCSchedule;
    constructor Create;
    procedure LoadFromFile(p: String);
    procedure SaveToFile(p: String);
  end;

implementation

constructor TNPCScheduleActivity.Create;
begin
  start_minute := 0;
  end_minute := 0;
  task_id := 0;
end;

procedure TNPCScheduleActivity.SetTaskId(val: LongWord);
begin
  task_id := val;
end;

procedure TNPCScheduleActivity.SetTime(s: LongWord; e: LongWord);
begin
  start_minute := s;
  end_minute := e;
end;


  //TNPCScheduleActivityList = class
  //  unk1: LongWord;
  //  raw_YearFlag: LongWord;
  //  list_size: LongWord;
  //  ScheduleActivities: array of TNPCScheduleActivity;
  //  procedure Create(size_of_list: Integer);

constructor TNPCScheduleActivityList.Create;
begin
  list_size := 0;
  SetLength(ScheduleActivities, 0);
end;

procedure TNPCScheduleActivityList.SetYearFlags(flags_to_set: LongWord);
var
  currBit: Integer;
begin
  for currBit := 0 to 7 do
  begin
    YearFlags[currBit] := False;
    if ((flags_to_set shr currBit) and 1) = 1 then
      YearFlags[currBit] := True;
  end;

  raw_YearFlags := flags_to_set;
end;

procedure TNPCScheduleActivityList.SetSchedFlags(flags_to_set: LongWord);
var
  currBit: Integer;
begin
  for currBit := 0 to 23 do
  begin
    SchedFlags[currBit] := False;
    if ((flags_to_set shr currBit) and 1) = 1 then
      SchedFlags[currBit] := True;
  end;

  raw_SchedFlags := flags_to_set;
end;

procedure TNPCScheduleActivityList.AddActivity(s, e, t: LongWord);
var
  I: Integer;
begin
   Inc(list_size);
   SetLength(ScheduleActivities, list_size);

   ScheduleActivities[list_size - 1] := TNPCScheduleActivity.Create;
   ScheduleActivities[list_size - 1].SetTime(s, e);
   ScheduleActivities[list_size - 1].SetTaskId(t);
end;

procedure TNPCScheduleActivityList.ResetActivities;
begin
  list_size := 0;
  SetLength(ScheduleActivities, list_size);
end;

//TNPCSchedule = class
//  schedule_size: LongWord;
//  ScheduleLists: array of TNPCScheduleActivityList;
//  procedure Create(size_of_schedule: Integer);
//end;

constructor TNPCSchedule.Create(size_of_schedule: Integer);
begin
  schedule_size := size_of_schedule;
  SetLength(ScheduleLists, schedule_size);
end;

//TNPCScheduleFile = class
//  file_size: LongWord;
//  Schedules: array of TNPCSchedule;
//  procedure Create(size_of_file: Integer);
//end;

constructor TNPCScheduleFile.Create;
begin
  file_size := 0;
  SetLength(Schedules, file_size);
end;

procedure TNPCScheduleFile.LoadFromFile(p: String);
var
  inFile: TMemoryStream;
  I, J, K: Integer;
  size_of_list: LongWord;
  SchedFlag1: LongWord;
  SchedFlag2: LongWord;
  act_start: LongWord;
  act_end: LongWord;
  task_id: LongWord;
begin
  inFile := TMemoryStream.Create;
  inFile.LoadFromFile(p);

  file_size := inFile.ReadDWord;
  SetLength(Schedules, file_size);

  for I := 0 to file_size - 1 do
  begin
    Schedules[I] := TNPCSchedule.Create(inFile.ReadDWord);

    if Schedules[I].schedule_size = 0 then
      Continue;

    for J := 0 to Schedules[I].schedule_size - 1 do
    begin
      SchedFlag1 := inFile.ReadDWord;
      SchedFlag2 := inFile.ReadDWord;
      size_of_list := inFile.ReadDWord;

      Schedules[I].ScheduleLists[J] := TNPCScheduleActivityList.Create;
      Schedules[I].ScheduleLists[J].SetYearFlags(SchedFlag2);
      Schedules[I].ScheduleLists[J].SetSchedFlags(SchedFlag1);

      for K := 0 to size_of_list - 1 do
      begin
        act_start := inFile.ReadDWord;
        act_end := inFile.ReadDWord;
        task_id := inFile.ReadDWord;

        Schedules[I].ScheduleLists[J].AddActivity(act_start, act_end, task_id);
      end;
    end;
  end;

  inFile.Free;
end;

procedure TNPCScheduleFile.SaveToFile(p: String);
var
  outFileMemStream: TMemoryStream;
  I, J, K: Integer;
begin
  outFileMemStream := TMemoryStream.Create;

  outFileMemStream.WriteDWord(file_size);

  for I := 0 to file_size - 1 do
  begin
    outFileMemStream.WriteDWord(Schedules[I].schedule_size);

    if Schedules[I].schedule_size = 0 then
      Continue;

    for J := 0 to Schedules[I].schedule_size - 1 do
    begin
      outFileMemStream.WriteDWord(Schedules[I].ScheduleLists[J].raw_YearFlags);
      outFileMemStream.WriteDWord(Schedules[I].ScheduleLists[J].raw_SchedFlags);

      outFileMemStream.WriteDWord(Schedules[I].ScheduleLists[J].list_size);

      for K := 0 to Schedules[I].ScheduleLists[J].list_size - 1 do
      begin
        outFileMemStream.WriteDWord(Schedules[I].ScheduleLists[J].ScheduleActivities[K].start_minute);
        outFileMemStream.WriteDWord(Schedules[I].ScheduleLists[J].ScheduleActivities[K].end_minute);
        outFileMemStream.WriteDWord(Schedules[I].ScheduleLists[J].ScheduleActivities[K].task_id);
      end;

    end;
  end;

  outFileMemStream.SaveToFile(p);
  outFileMemStream.Free;
end;

end.

