unit awlMessage;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fileInOut, LazLogger, LazUTF8, soshelpers;

function ConvertFromHM(Message: array of byte; mIndex: Integer): string;

type
  TGameType = (sosawl, awl, anwl);

  TMsg = record
    Index: Integer;
    mTitle: WideString;
    mText: WideString;
  end;

  TMes = class
  private
    procedure GetPointersHM;
    procedure GetPointersSoS;    
    procedure GetAllMessagesHM;
    procedure GetAllMessagesSoS;  
    procedure ReadMessage(I, Start, Len: Integer);
  public                 
    Path: String;
    NumMsg: Integer;
    MsgCollection: array of TMsg;
    HasUnkChars: boolean;
    GameType: TGameType;           
    mesFileStream: TMemoryStream;
    msgPointers: array of UInt32;
    msgTitlePointers: array of UInt32;

    constructor Create(p: String);          
    procedure LoadFromFile;
    procedure writeFile(o: string);
    procedure GetSizeOfAllMessages;
    procedure BuildMemoryStreams;
    procedure BuildFileStream(p: string);
  end;


const
  AWLCharMap: array[$80..$84,$00..$FF] of String = (
                    {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {A} {B} {C} {D} {E} {F}
           {80} ({0}'0','1','2','3','4','5','6','7','8','9','-','A','B','C','D','E',
                 {1}'F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U',
                 {2}'V','W','X','Y','Z','あ','い','う','え','お','か','き','く','け','こ','さ',
                 {3}'し','す','せ','そ','た','ち','つ','て','と','な','に','ぬ','ね','の','は','ひ',
                 {4}'ふ','へ','ほ','ま','み','む','め','も','や','ゆ','よ','ら','り','る','れ','ろ',
                 {5}'わ','を','ん','ぁ','ぃ','ぅ','ぇ','ぉ','ゃ','ゅ','ょ','っ','が','ぎ','ぐ','げ',
                 {6}'ご','ざ','じ','ず','ぜ','ぞ','だ','ぢ','づ','で','ど','ば','び','ぶ','べ','ぼ',
                 {7}'ぱ','ぴ','ぷ','ぺ','ぽ','ア','イ','ウ','エ','オ','カ','キ','ク','ケ','コ','サ',
                 {8}'シ','ス','セ','ソ','タ','チ','ツ','テ','ト','ナ','ニ','ヌ','ネ','ノ','ハ','ヒ',
                 {9}'フ','ヘ','ホ','マ','ミ','ム','メ','モ','ヤ','ユ','ヨ','ラ','リ','ル','レ','ロ',
                 {A}'ワ','ヲ','ン','ア','イ','ウ','エ','オ','ャ','ュ','ョ','ッ','ガ','ギ','グ','ゲ',
                 {B}'ゴ','ザ','ジ','ズ','ゼ','ゾ','ダ','ヂ','ヅ','デ','ド','バ','ビ','ブ','ベ','ボ',
                 {C}'ヴ','パ','ピ','プ','ペ','ポ','+','×','.','○','?','!','●','♂','♀','·',
                 {D}'—','&"','/','♪','☆','★','♥','%','a','b','c','d','e','f','g','h',
                 {E}'i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x',
                 {F}'y','z','''','<','>','(',')','｢','｣','~','*',' ',' ','ä','ö','ü'),

                    {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {A} {B} {C} {D} {E} {F}
           {81} ({0}'Ä','Ö','Ü','β','"',',',':','昔','使','刄','毎','日','入','意','味','何',
                 {1}'役','立','合','掘','場','所','客','私','名','考','古','学','研','究','現','行',
                 {2}'助','教','沈','今','終','0' ,'貴','事','書','石','板','見','礼','言','特','物',
                 {3}'以','上','切','君','良','送','実','通','什','之','明','信','伝','廿','神','条',
                 {4}'件','必','出','子','森','番','人','頭','友','予','感','育','0' ,'0' ,'歌','声',
                 {5}'足','星','住','生','幸','勢','ヵ' ,'寝','坊','毛','0' ,'年','案','内','全','体',
                 {6}'0' ,'中','土','品','画','時','途','了','0' ,'0' ,'食','説','授','落','半','思',
                 {7}'主','公','0' ,'0' ,'楽','仕','恋','且','玉','付','顔','完','返','不','可','司',
                 {8}'呼','新','0' ,'支','長','世','社','0' ,'0' ,'0' ,'変','回','配','休','戻','登',
                 {9}'曲','本','閤','弱','虫','強','用','当','団','供','祖','負','待','囗','決','最',
                 {A}'近','男','昼','油','売','伺','木','他','父','読','母','国','王','息','首','交',
                 {B}'互','氷','葉','定','校','勉','按','散','ニ','走','若','寄','泣','貸','…' ,'引',
                 {C}'関','係','0' ,'苦','愛','退','屈','会','0' ,'守','逆','丈','夫','軽','0', '0' ,
                 {D}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {E}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {F}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'),

                    {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {A} {B} {C} {D} {E} {F}
           {82} ({0}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {1}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {2}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {3}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {4}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {5}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {6}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {7}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {8}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {9}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {A}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {B}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {C}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {E}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {E}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {F}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'),

                    {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {A} {B} {C} {D} {E} {F}
           {83} ({0}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {1}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {2}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {3}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {4}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {5}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {6}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {7}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {8}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {9}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {A}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {B}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {C}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {E}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {E}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {F}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'),

                    {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {A} {B} {C} {D} {E} {F}
           {84} ({0}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {1}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {2}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {3}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {4}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {5}'0','0','0','0','0','0','0','0','0','"',',',':','ä','ö','ü','Ä',
                 {6}'Ö','Ü','β','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {7}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {8}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {9}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {A}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {B}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {C}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {E}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {E}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',
                 {F}'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0')
  );

  AnWLCharMap: array[$60..$C8] of String = ( // Character map for Another Wonderful Life, which used single-byte characters
                    {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {A} {B} {C} {D} {E} {F}
                 {6}'0','1','2','3','4','5','6','7','8','9','-','A','B','C','D','E',
                 {7}'F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U',
                 {8}'V','W','X','Y','Z','+','×','.','○','?','!','●','♂','♀','•','-',
                 {9}'&','/','♪','★','☆','♥','%','a','b','c','d','e','f','g','h','i',
                 {A}'j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y',
                 {B}'z','''','<','>','(',')','｢','｣','~','✽','“','”','_','-','=',',',
                 {C}':','α','θ','ψ','Α','Θ','Ψ','β','‾'
                 );

  NameMap: array [$00..$26] of String = (
                    'Pete','Tatsuya','Celia','Muffy','Nami','Murrey','Carter',
                    'Takakura','Romana','Lumina','Sebastian','Wally','Chris',
                    'Hugh','Grant','Samantha','Kate','Galen','Nina','Daryl',
                    'Gustafa','Cody','Kassey','Patrick','Tim','Ruby','Rock',
                    'Griffin','Flora','Vesta','Marlin','Hardy','Nak','Nic',
                    'Flak','Mukumuku','Van','DUMMY','Tartan'
                    );

var
  mes: TMes;
  readAsAnWL: boolean = false;
  expectedFileSize: UInt64;
  expectedIndexSize: UInt64;
  expectedMsgSize: UInt64;
  expectedTitleSize: UInt64;

  outMsgStream: TMemoryStream = nil;
  outTitleStream: TMemoryStream = nil;
  outIndexStream: TMemoryStream = nil;
  outFileStream: TFileStream = nil;

implementation

constructor TMes.Create(p: String);
begin
  Path := p;
  mesFileStream := getFile(Path);

  case ExtractFileExt(Path) of
    '.bin': GameType := sosawl;
    '.mes': GameType := awl;
  end;
end;

procedure TMes.LoadFromFile;
begin
  // Read the pointer table, then build our arrays
  if GameType = sosawl then
    begin
      GetPointersSoS;
      GetAllMessagesSoS;
    end
  else
    begin
      GetPointersHM;
      GetAllMessagesHM;
    end;
end;


procedure TMes.GetPointersSoS;
var
    I: Integer = 0;
    currPointer: UInt64;
begin
  // The new SoS AWL remake uses .bin files that are descendants, but differ
  // from their HM ancestors in some pretty good ways.
  mesFileStream.Position := $0;
  currPointer := mesFileStream.ReadDWord;
  NumMsg := (currPointer div 16);
  debugLn('Number of messages: ' + IntToStr(NumMsg));

  SetLength(msgPointers,NumMsg);
  SetLength(msgTitlePointers,NumMsg);

  // Now we just read the pointers and throw them into the array
  while I < (NumMsg) do
    begin
      mesFileStream.Position := (16 * I);
      msgPointers[I] := mesFileStream.ReadDWord;
      mesFileStream.Position := (16 * I) + 4;
      msgTitlePointers[I] := mesFileStream.ReadDWord;
      I := I + 1;
    end;
end;

procedure TMes.GetPointersHM;
var
  I: Integer = 0;
begin
  // .mes files graciously tell us the number of MsgContent contained within
  mesFileStream.Position := $6;
  mesFileStream.ReadBuffer(NumMsg,2);
  // Gamecube uses Big Endian, need to convert that to the system's native
  NumMsg := BEToN(NumMsg);
  SetLength(msgPointers,NumMsg);

  debugLn('Number of messages: ' + IntToStr(NumMsg));

  // Now we just read the pointers and throw them into the array
  I := 0;
  while I < (NumMsg) do
    begin
      // $8 is the location of the first pointer. Four bytes long, Big Endian
      mesFileStream.Position := $8 + (4 * I);
      mesFileStream.ReadBuffer(msgPointers[I],4);
      I := I + 1;
    end;
//  debugLn('Size of PointerTable: ' + IntToStr(Length(msgPointers)));
end;

procedure TMes.GetAllMessagesSoS;
var
  I: Integer = 0;
  Loc: UInt64 = 0;
  Len: UInt64 = 0;
  currMsg: TMsg;            
  stringBytes: array of Integer;
begin            
  // Resize the array of our message records
  setLength(MsgCollection,0);
  setLength(MsgCollection,NumMsg);

  for I := 0 to NumMsg - 1 do
    begin
      with MsgCollection[I] do
      begin
        Loc := msgPointers[I];
        if I = NumMsg - 1 then
          Len := msgTitlePointers[0] - Loc
        else
          Len := msgPointers[I + 1] - Loc;

        Index := I;

        SetLength(stringBytes, Len);
        mesFileStream.Seek(Loc, 0);
        //mesFileStream.ReadBuffer(mText, Len);
        mesFileStream.ReadBuffer(stringBytes[0], Len);
        SetString(mText, PWideChar(@stringBytes[0]), Len div 2);


        Loc := msgTitlePointers[I];
        if I = NumMsg - 1 then
          Len := mesFileStream.Size - Loc
        else
          Len := msgTitlePointers[I + 1] - Loc;

        Index := I;

        SetLength(stringBytes, Len);
        mesFileStream.Seek(Loc, 0);
        mesFileStream.ReadBuffer(stringBytes[0], Len);
        //mesFileStream.ReadBuffer(mTitle, Len);
        SetString(mTitle, PWideChar(@stringBytes[0]), Len div 2);
      end;
    end;
end;

procedure TMes.GetAllMessagesHM;
var
  I: Integer = 0;
  Loc: Integer = 0;
  Len: Integer = 0;
begin
  // Resize the array of our message records
  setLength(MsgCollection,0);
  setLength(MsgCollection,NumMsg);

  for I := 0 to NumMsg - 1 do
    begin
      with MsgCollection[I] do
      begin
        Loc := BEToN(msgPointers[I]);
        if I = NumMsg - 1 then
          Len := mesFileStream.Size - Loc
        else
          Len := BEToN(msgPointers[I + 1]) - Loc;

        Index := I;
        ReadMessage(I, Loc, Len);
        //mText := ConvertFromHM(mBytes, I);
        debugLn('Msg ' + IntToStr(I) + ': ' + mText);
      end;
    end;
end;

// Given a starting location, this will read byte-by-byte until it reaches
// a $30 or $00 followed by an $00 byte, at which point it will exit. Recursive.
procedure TMes.ReadMessage(I, Start, Len: Integer);
var
  b: byte;
begin
  with MsgCollection[I] do
  begin
    mesFileStream.Position := Start;
    //mes.mesFileStream.ReadBuffer(mBytes[0], Len);
    //for b in mBytes do
      //mByteStr := mByteStr + IntToHex(b, 2) + ' '
  end;
end;


procedure TMes.writeFile(o: string);
begin
  GetSizeOfAllMessages;
  BuildMemoryStreams;
  BuildFileStream(o);
end;

procedure TMes.GetSizeOfAllMessages;
var
  currMsg: TMsg;
begin
  expectedIndexSize := 0;
  expectedMsgSize := 0;
  expectedTitleSize := 0;
  expectedFileSize := 0;

  // File size will be:
  //     16 * number of messages (index size)
  //     Length (in bytes) of all messages, summed
  //     Length (in bytes) of all message titles, summed

  expectedIndexSize := (16 * Mes.NumMsg);

  for currMsg in Mes.MsgCollection do
  begin
    expectedMsgSize := expectedMsgSize + (Length(currMsg.mText) * 2);
    expectedTitleSize := expectedTitleSize + (Length(currMsg.mTitle) * 2);
  end;

  expectedFileSize := expectedIndexSize + expectedMsgSize + expectedTitleSize;

  debugLn('Calculated size: ' + IntToStr(expectedFileSize));
end;

procedure TMes.BuildMemoryStreams;
var
  currMsg: TMsg;
  msgOffset: Integer;
  titleOffset: Integer;
  I: Integer = 0;
  msgPointerOffset: Integer = 0;
  indexMsgPointer: Integer = 0;
  indexTitlePointer: Integer = 0;
  tempStr: PWideChar;
begin
  outIndexStream := TMemoryStream.Create();
  outMsgStream := TMemoryStream.Create();
  outTitleStream := TMemoryStream.Create();

  // We know where the message block will start, immediately following the Index
  // which has a known, calculatable size
  msgOffset := expectedIndexSize;

  // We already got the size of each block as well, so we can calculate the
  // starting location of the Title block
  titleOffset := expectedIndexSize + expectedMsgSize;

  // Iterate through each Message
  for currMsg in Mes.MsgCollection do
  begin
    // Write the current value of msgOffset and titleOffset to the Index Stream
    // then write a quad word of 0 to finish the index entry.
    outIndexStream.WriteDWord(msgOffset);
    outIndexStream.WriteDWord(titleOffset);
    outIndexStream.WriteQWord(0);

    // Now we just append the Message and Title values to their relevant streams
    outMsgStream.WriteBuffer(ConvertUTF8ToUTF16(currMsg.mText, False)[1], Length(currMsg.mText) * 2);
    outTitleStream.WriteBuffer(ConvertUTF8ToUTF16(currMsg.mTitle, False)[1], Length(currMsg.mTitle) * 2);

    // Finally, increment our offsets for the next round
    msgOffset := msgOffset + (Length(currMsg.mText) * 2);
    titleOffset := titleOffset + (Length(currMsg.mTitle) * 2);
  end;
end;

procedure TMes.BuildFileStream(p: String);
begin
  outFileStream := TFileStream.Create(p, fmCreate);
  outFileStream.CopyFrom(outIndexStream, 0);
  outFileStream.CopyFrom(outMsgStream, 0);
  outFileStream.CopyFrom(outTitleStream, 0);

  outFileStream.Destroy;
end;

function ConvertFromHM(Message: array of byte; mIndex: Integer): string;
var
  I: Integer = 0;
  ReadChar: Byte;
  // Result: String = '';
  Append: String = '';
  Incre: Integer;
begin
  ConvertFromHM := '';
  while I < Length(Message) do
    begin
      Append := '';
      Incre := 1;
      if (readAsAnWL = false) and (Message[I] in [$80..$84]) then
        // It's an AWL character
        begin
          ReadChar := Message[I + 1];
          Incre := 2;
          Append := AWLCharMap[Message[I]][ReadChar];
        end
      else if (readAsAnWL = true) and (Message[I] in [$60..$C8]) then
        begin
          // It's an AnWL character
          ReadChar := Message[I];
          Append := AnWLCharMap[Message[I]];
        end
      else
        // It's a special marker
        begin
          case Message[I] of
            $00: Append := '{EOM}';               // End of message marker, when not a part of anything else
            $01: Append := sLineBreak;            // Line break
            $02: Append := ' ';                   // Space
            $03: Append := '{ENDPAGE}';           // Page end marker
            $10..$17:                             // Colors
                 Append := '{C' + IntToHex(Message[I],2) + '}';
            { $14: begin                            // Farm name? Seems to be various location names.
                   Append := '{LOCATION}';
                   Incre := 4;
                 end; }
            $18: begin                            // Unknown. See string.mes message 54
                   Append := '{?_' + IntToHex(Message[I],2) + IntToHex(Message[I + 2],2) + '}';
                   Incre := 2;
                 end;
            $20: begin                            // People. Pulls from people.mes
                   ReadChar := Message[I + 1];
                   if Message[I + 1] in [$00..$26] then
                     begin
                       Append := '{' + UpperCase(NameMap[ReadChar]) + '}';
                     end
                   else
                     Append := '{CHAR_'+ IntToHex(Message[I + 1],2) + '}';       // Odd case where it's not in the array
                   Incre := 2;
                 end;
            $21: begin                            // Previous input? Seems so.
                   Append := '{PREV_' + IntToHex(Message[I + 1],2) + '}';
                   Incre := 2;
                 end;
            $22: begin                            // A name -- not sure whose, but I think an alternate son's name?
                   Append := '{NAME?}';           // romana.mes message 249, muumuu_wife.mes message 59, etc. Only said by romana and wife
                 end;
            $23: begin                            // Dog name, see son_talk.mes
                   Append := '{DOG}';
                 end;
            $24: begin
                   Append := '{FARM}';
                 end;
            $25: begin                            // Item name from memory. Related to record player and others? Possbly from item.mes
                   Append := '{ITEM_' + IntToHex(Message[I + 1],2) + IntToHex(Message[I + 2],2)+'}';
                   Incre := 3;
                 end;
            $26: begin                            // Crops from farmcrop.mes
                   Append := '{CROP_' + IntToHex(Message[I + 1],2) + IntToHex(Message[I + 2],2) + '}';
                   Incre := 3;
                 end;
            $27: begin                            // Seems to be Ordered Items. 3 bytes.
                   Append := '{ORD_' + IntToStr(Message[I + 2]) + '}';
                   Incre := 3;
                 end;
            $28: begin                            // Chapter titles, not sure where they pull from. other.mes 450+
                   Append := '{Title_' + IntToStr(Message[I + 2]) + '}';
                   Incre := 3;
                 end;
            $29: begin                            // Variable marker. 2 bytes
                   //ReadChar := Message[I + 1];  // Apparently texts can be passed variables
                   Append := '{VAR_' + IntToHex(Message[I + 1],2) + '}';
                   Incre := 2;
                 end;
            $2A: begin                            // Money ** MAYBE NOT. Seems to be anything numeric. 3 bytes
                   Append := '{GOLD_' + IntToHex(Message[I + 1],2) + IntToHex(Message[I + 2],2) + '}';
                   Incre := 3;
                 end;
            $2B: begin                            // Pulls from structure.mes. 2 bytes.
                   Append := '{STRUC_' + IntToStr(Message[I + 1]) + '}';
                   Incre := 2;
                 end;
            $2C: begin                            // Unknown -- seems to pull in untranslated phrases from somewhere -- see phrase.mes message 28
                   Append := '{PH_' + IntToHex(Message[I + 1],2) + IntToHex(Message[I + 2],2) + '}';
                   Incre := 3;
                 end;
            $2D: begin                            // Nickname that spouse calls player
                   Append := '{NICKNAME}';
                 end;
            $30: begin                            // Pause
                   Append := '{PAUSE}';
                 end;
            $31: begin                            // Clears the current message board, next part comes in at once.
                   Append := '{WIPE}';
                 end;
            $32,$34:                              // Sound. 3 bytes. Probably also $33? Haven't seen it though
                 begin
                   Incre := 3;
                   Append := '{S_' + IntToHex(Message[I],2) + IntToHex(Message[I + 1],2) + IntToHex(Message[I + 2],2) + '}';
                 end;
            $33: begin                            // It looks like this makes the text pause halfway through, see flora.mes message 82, 84
                   Incre := 3;
                   Append := '{P_' + IntToHex(Message[I],2) + IntToHex(Message[I + 1],2) + IntToHex(Message[I + 2],2) + '}';
                 end;
            $35: begin                            // Related to how the text box is drawn?
                   Incre := 2;                    // flat.mes, Message 48
                   Append := '{DRAW_' + IntToHex(Message[I + 1],2) + '}';  // Looks like $3502 makes it draw character-by-character, with noise. $3500 resets to quiet
                 end;
            $36: begin                            // Makes all text draw at once, instead of character at a time.
                   Append := '{SKIP}';
                 end;
            $37: begin                            // Something related to debug? I only see it in debug.mes, 326, 350-394
                   Append := '{DBG_' +  IntToHex(Message[I],2) + IntToHex(Message[I + 1],2) + IntToHex(Message[I + 2],2) + IntToHex(Message[I + 3],2) + IntToHex(Message[I + 4],2) + '}';
                   Incre := 5;
                 end;
            $38: begin                            // Unsure, seems to pull in strings from somewhere else. See boy.mes
                   Append := '{?_' + IntToHex(Message[I],2) + IntToHex(Message[I + 2],2) + '}';
                   Incre := 2;
                 end;
            $39: begin                            // Unsure, something that spouses do at some point? See nami.mes Message 299, other.mes Message 367?
                   Append := '{?_' + IntToHex(Message[I],2) + '}';
                   Incre := 1;
                 end;
            $40: begin                            // Simple Yes/No choice
                   Append := '{CHOICE Y/N DEF' + IntToStr(Message[I + 1]) + '}';      // Second byte is default choice
                   Incre := 2;
                 end;
            $41: begin                            // Custom Player choice
                   ReadChar := Message[I + 1];    // Third byte is default choice, following lines are options
                   Append := '{CHOICE' + IntToStr(Message[I + 1]) + ' DEF' + IntToStr(Message[I + 2]) + '}' + sLineBreak;
                   Incre := 3;
                 end;
            $50: begin                            // Seems to change facial expressions?
                   Append := '{FACE_' + IntToHex(Message[I + 1],2) + IntToHex(Message[I + 2],2) + IntToHex(Message[I + 3],2) + '}';
                   Incre := 4;
                 end;
            $52: begin                            // Unsure -- see Message 380 in muumuu.mes (four bytes of $52 00 00 00 at offset 9936)
                   Append := '{FACER_' + IntToHex(Message[I + 1],2) + IntToHex(Message[I + 2],2) + IntToHex(Message[I + 3],2) + '}';     // It follows a face set,  so I'm assuming this is reset
                   Incre := 4;
                 end;
            else                                  // Unknown byte
              begin
                Append := '{' + IntToHex(Message[I],2) + '}';
                //Mes.MsgCollection[mIndex].mBytesUnknown := true;
                //HasUnkChars := true;
              end;
          end;
        end;
      ConvertFromHM := ConvertFromHM + Append;
      Inc(I,Incre);
    end;
end;

end.

