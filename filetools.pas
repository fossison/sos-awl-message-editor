unit filetools;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls,
  fileInOut, lzsTools, lazLogger, LazUTF8;


procedure SplitRiff(p: String; be: Boolean = false);
type

  { TFileToolsForm }

  TFileToolsForm = class(TForm)
    Button1: TButton;
    DcmpToLzsButton: TButton;
    Label2: TLabel;
    ScriptToolsGroupBox: TGroupBox;
    Label1: TLabel;
    PackFolderButton: TButton;
    ExtractDcmpButton: TButton;
    ExePatchesGroupBox: TGroupBox;
    ExePatchInstructionLabel: TLabel;
    ExtractLzsButton: TButton;
    LzsToolsGroupBox: TGroupBox;
    LoadUnpackedCheckbox: TCheckBox;
    ApplyToExeButton: TButton;
    ExeSaveDialog: TSaveDialog;
    OpenDialog1: TOpenDialog;
    SelectDirectoryDialog1: TSelectDirectoryDialog;
    procedure ApplyToExeButtonClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure DcmpToLzsButtonClick(Sender: TObject);
    procedure ExtractDcmpButtonClick(Sender: TObject);
    procedure ExtractLzsButtonClick(Sender: TObject);
    procedure PackFolderButtonClick(Sender: TObject);
    procedure patchExe(Path: String);
  private

  public

  end;

  TRiffChunk = class
    identifier: String[4];
    length: Word;
    form_type: string[4];
    chunk_data: TMemoryStream;
  end;

const
  loadPath = './disc/';

var
  FileToolsForm: TFileToolsForm;
  exePath: String;
  exeFileStream: TFileStream;
  lzsOpened: TLzs;

implementation

{$R *.lfm}

procedure SplitRiff(p: String; be: Boolean = false);
var
  ScriptBin: TMemoryStream;
  chunk_header: string[4] = '';
  chunk_size: LongWord;
  form_type: string[4] = '';
  chunk2_header: string[4] = '';
  chunk2_size: LongWord;
  outFolder: string;
  outFile: TFileStream;
  I: Integer;
begin
  ScriptBin := TMemoryStream.Create;
  ScriptBin.LoadFromFile(p);
  ScriptBin.Position := 0;

  for I := 0 to 3 do
    chunk_header := chunk_header + chr(ScriptBin.ReadByte);

  chunk_size := BEToN(ScriptBin.ReadDWord);

  if chunk_header = 'RIFF' then
  begin
    DebugLn('It''s a RIFF');
    for I := 0 to 3 do
      form_type := form_type + chr(ScriptBin.ReadByte);


    DebugLn('Form type: ' + form_type);

    outFolder := Trim(p + '_' + form_type);
    debugLn('Out folder: ' + outFolder);

    CreateDir(outFolder);

    while ScriptBin.Position < ScriptBin.Size do
    begin
      chunk2_header := '';
      for I := 0 to 3 do
        chunk2_header := chunk2_header + chr(ScriptBin.ReadByte);

      DebugLn('Next chunk header: ' + chunk2_header);
      chunk2_size := BEToN(ScriptBin.ReadDWord);

      outFile := TFileStream.Create(outFolder + '/' + chunk2_header, fmCreate);
      outFile.CopyFrom(ScriptBin, chunk2_size);
      outFile.Free;
    end;
  end;

end;

{ TFileToolsForm }

procedure TFileToolsForm.ApplyToExeButtonClick(Sender: TObject);
begin
  if ExeSaveDialog.Execute then
    begin
      createBackupFile(ExeSaveDialog.FileName);
      patchExe(ExeSaveDialog.FileName);
    end;
end;

procedure TFileToolsForm.Button1Click(Sender: TObject);
begin
  if OpenDialog1.Execute then
    begin
      DebugLn('Opening file, splitting riff...');
      SplitRiff(OpenDialog1.FileName);
    end;
end;

procedure TFileToolsForm.DcmpToLzsButtonClick(Sender: TObject);
var
  lzsOut: TLzs;
begin
  if OpenDialog1.Execute then
  begin
    lzsOut := TLzs.Create;
    lzsOut.CompressFile(OpenDialog1.FileName);
  end;
end;

procedure TFileToolsForm.ExtractDcmpButtonClick(Sender: TObject);
var
  dcmpFile: TDcmp;
  folderPath: String;
begin
  if OpenDialog1.Execute then
  begin
    dcmpFile := TDcmp.Create;
    dcmpFile.LoadDcmpFromFile(OpenDialog1.FileName);
    folderPath := UTF8toSys(ExtractFilePath(OpenDialog1.FileName) + ExtractFileName(OpenDialog1.FileName));
    folderPath := folderPath + '_out/';
    if not DirectoryExists(folderPath) then
    begin
      DebugLn('Creating Directory ' + folderPath);
      if CreateDir(folderPath)
      then ShowMessage('New directory added OK')
      else ShowMessage('New directory add failed with error : ' + IntToStr(GetLastOSError()));
    end;
    dcmpFile.saveToFolder(folderPath);
  end;
end;

procedure TFileToolsForm.ExtractLzsButtonClick(Sender: TObject);
begin
  if OpenDialog1.Execute then
  begin
    DebugLn('Opening ' + OpenDialog1.FileName);
    lzsOpened := TLzs.Create;
    lzsOpened.LoadFromLzFile(OpenDialog1.FileName);
    DebugLn('Opened ' + OpenDialog1.FileName + ', decoding...');
    lzsOpened.DecodeLzStream;
    DebugLn('Decoded, saving...');
    lzsOpened.SaveDecStreamToFile(OpenDialog1.FileName + '.dcmp');
    DebugLn('Saved!');
  end;
end;

procedure TFileToolsForm.PackFolderButtonClick(Sender: TObject);
var
  DcmpOut: TDcmp;
begin
  if SelectDirectoryDialog1.Execute then
  begin
    DcmpOut := TDcmp.Create;
    DcmpOut.FolderToDcmp(SelectDirectoryDialog1.FileName);
  end;
end;

procedure TFileToolsForm.patchExe(Path: String);
var
  lengthOfPath: Integer;
begin
  exeFileStream := TFileStream.Create(Path, fmOpenReadWrite);

  if LoadUnpackedCheckbox.Checked then
    begin
      lengthOfPath := Length(loadPath);
      //exeFileStream.Position := $5691bd; v1
      exeFileStream.Position := $568E2D; // v2
      exeFileStream.Write(lengthOfPath, 1);

      //exeFileStream.Position := $BE86A8; // v1
      exeFileStream.Position := $BEA0C8; // v2
      exeFileStream.Write(loadPath, lengthOfPath);
    end;

  exeFileStream.Free;
end;

end.

