unit Writing;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, fileInOut, LazLogger, LazUTF8, LazUnicode,
  Reading, dutf8, dutf16;
                                     
type
  TUTF16Bytes = array of PWideChar;

procedure writeFile(Path: string);
procedure GetSizeOfAllMessages;
procedure BuildMemoryStreams;
procedure BuildFileStream(Path: string);
function ConvertUTF8ToUTF16(const S: AnsiString; BigEndian: Boolean = True): WideString;


var
  expectedFileSize: UInt64;
  expectedIndexSize: UInt64;
  expectedMsgSize: UInt64;
  expectedTitleSize: UInt64;

  outMsgStream: TMemoryStream = nil;  
  outTitleStream: TMemoryStream = nil;   
  outIndexStream: TMemoryStream = nil;
  outFileStream: TFileStream = nil;


implementation

procedure writeFile(Path: string);
begin
  //outFileStream := TFileStream.Create(Path);

  GetSizeOfAllMessages;
  BuildMemoryStreams;
  BuildFileStream(Path);
end;

procedure GetSizeOfAllMessages;
var
  currMsg: TMsg;
begin
  expectedIndexSize := 0;
  expectedMsgSize := 0;
  expectedTitleSize := 0;
  expectedFileSize := 0;

  // File size will be:
  //     16 * number of messages (index size)
  //     Length (in bytes) of all messages, summed
  //     Length (in bytes) of all message titles, summed

  expectedIndexSize := (16 * Mes.NumMsg);

  for currMsg in Mes.MsgCollection do
  begin
    expectedMsgSize := expectedMsgSize + (Length(currMsg.mText) * 2);
    expectedTitleSize := expectedTitleSize + (Length(currMsg.mTitle) * 2);
  end;

  expectedFileSize := expectedIndexSize + expectedMsgSize + expectedTitleSize;

  debugLn('Calculated size: ' + IntToStr(expectedFileSize));
end;

procedure BuildMemoryStreams;
var  
  currMsg: TMsg;
  msgOffset: Integer;
  titleOffset: Integer;
  I: Integer = 0;
  msgPointerOffset: Integer = 0;
  indexMsgPointer: Integer = 0;
  indexTitlePointer: Integer = 0;
  tempStr: PWideChar;
begin
  outIndexStream := TMemoryStream.Create();
  outMsgStream := TMemoryStream.Create();
  outTitleStream := TMemoryStream.Create();

  // We know where the message block will start, immediately following the Index
  // which has a known, calculatable size
  msgOffset := expectedIndexSize;

  // We already got the size of each block as well, so we can calculate the
  // starting location of the Title block
  titleOffset := expectedIndexSize + expectedMsgSize;

  // Iterate through each Message
  for currMsg in Mes.MsgCollection do
  begin
    // Write the current value of msgOffset and titleOffset to the Index Stream
    // then write a quad word of 0 to finish the index entry.
    outIndexStream.WriteDWord(msgOffset);
    outIndexStream.WriteDWord(titleOffset);
    outIndexStream.WriteQWord(0);

    // Now we just append the Message and Title values to their relevant streams
    outMsgStream.WriteBuffer(ConvertUTF8ToUTF16(currMsg.mText, False)[1], Length(currMsg.mText) * 2);
    outTitleStream.WriteBuffer(ConvertUTF8ToUTF16(currMsg.mTitle, False)[1], Length(currMsg.mTitle) * 2);

    // Finally, increment our offsets for the next round
    msgOffset := msgOffset + (Length(currMsg.mText) * 2);
    titleOffset := titleOffset + (Length(currMsg.mTitle) * 2);
  end;
end;

procedure BuildFileStream(Path: String);
begin
  outFileStream := TFileStream.Create(Path, fmCreate);
  outFileStream.CopyFrom(outIndexStream, 0);
  outFileStream.CopyFrom(outMsgStream, 0);
  outFileStream.CopyFrom(outTitleStream, 0);

  outFileStream.Destroy;
end;
            
//  Source: https://github.com/visualdoj/dunicode
//  License: Unknown, not listed.
//  Converts UTF-8 string to UTF-16BE (by default) or UTF-16LE.
//
//  Replaces all malformed data in S to '?'.
//
function ConvertUTF8ToUTF16(const S: AnsiString; BigEndian: Boolean = True): WideString;
var
  L: SizeUInt;
  U: Cardinal;
  SCursor, SCursorEnd: PAnsiChar;
  WCursor, WCursorEnd: PWideChar;
begin
  // In the first pass determine the size of resulting string.
  L := 0;
  SCursor := @S[1];
  SCursorEnd := SCursor + Length(S);
  while DecodeUTF8CharReplace(SCursor, SCursorEnd, U, Ord('?')) do
    Inc(L, GetUTF16CharSize(U));
  // GetUTF16CharSize returns number of bytes.
  // But we want to compute number of WideChars. SizeOf(WideChar) = 2.
  L := L div 2;
  // In the second pass encode resulting UTF-16 string.
  SetLength(Result, L);
  SCursor := @S[1];
  SCursorEnd := SCursor + Length(S);
  WCursor := @Result[1];
  WCursorEnd := WCursor + Length(Result);
  while DecodeUTF8CharReplace(SCursor, SCursorEnd, U, Ord('?')) do
    EncodeUTF16Char(U, WCursor, WCursorEnd, BigEndian);
end;
end.

