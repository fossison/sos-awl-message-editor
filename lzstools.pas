unit lzsTools;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, LazUTF8,
  LazLogger, FileUtil;

type

  { TLzsToolsForm }

  TDcmpFileEntry = record
    dummy1: Word;
    dummy2: Word;
    offset: LongWord;
    size: LongWord;
    id: LongWord;
    name: String[48];
    fileStream: TMemoryStream;
  end;

  TDcmp = class
    numFiles: Word;
    inStream: TMemoryStream;
    indexStream: TMemoryStream;
    fileEntries: array of TDcmpFileEntry;
  private
    procedure LoadFiles;
  public
    constructor Create;
    procedure LoadDcmpFromFile(p: String);
    procedure SaveToFolder(p: String);
    procedure FolderToDcmp(f: String);
  end;

  TLzs = class
    lzType: ShortInt;
    decLength: Integer;
    lzStream: TMemoryStream;
    decStream: TMemoryStream;
    procedure Extend(var int: longWord);
  private
    procedure GetLzTypeAndSize;
    procedure EncodeLzss;
    procedure DecodeLzss;
    procedure DecodeLz4;
    procedure WriteLzFirstWord;
  public
    constructor Create;
    procedure LoadFromLzFile(p: String);
    procedure SaveLzStreamToFile(p: String);
    procedure SaveDecStreamToFile(p: String);
    procedure DecodeLzStream;
    procedure CompressFile(p: String; lz_type: Integer = 0);
  end;

  TLzsToolsForm = class(TForm)
    ExtractLzsButton: TButton;
    Label1: TLabel;
    OpenDialog1: TOpenDialog;
  private

  public

  end;

var
  LzsToolsForm: TLzsToolsForm;   
  lzsStream: TMemoryStream;
  outStream: TMemoryStream;
  outFileStream: TFileStream;

const
  LZSS_LOOKBACK_WINDOW: Integer = 4096;

implementation

{$R *.lfm}

{ TDcmp }

constructor TDcmp.Create;
begin
  // LoadDcmpFromFile(p);
end;

procedure TDcmp.LoadDcmpFromFile(p: String);
begin
  inStream := TMemoryStream.Create();
  inStream.LoadFromFile(p);
  LoadFiles;
end;

procedure TDcmp.LoadFiles;
var
  currEntry: TDcmpFileEntry;
  I, J: Integer;
  outFile: TFileStream;
  currChar: Char;
begin
  inStream.Position := 0;
  numFiles := inStream.ReadWord;
  SetLength(fileEntries, numFiles);

  inStream.Position := 0;
  indexStream := TMemoryStream.Create;
  indexStream.CopyFrom(inStream, (numFiles * 64));

  indexStream.Position := 0;
  for I := 0 to numFiles - 1 do
  begin
    indexStream.Position := (I * 64);

    fileEntries[I].dummy1 := indexStream.ReadWord;
    fileEntries[I].dummy2 := indexStream.ReadWord;
    fileEntries[I].offset := indexStream.ReadDWord;
    fileEntries[I].size   := indexStream.ReadDWord;
    fileEntries[I].id     := indexStream.ReadDWord;

    fileEntries[I].name := '';
    for J := 1 to 48 do
    begin
      indexStream.ReadBuffer(currChar, 1);
      fileEntries[I].name := fileEntries[I].name + currChar;
    end;

    fileEntries[I].fileStream := TMemoryStream.Create;
    inStream.Position := fileEntries[I].offset;
    fileEntries[I].fileStream.CopyFrom(inStream, fileEntries[I].size);
  end;
end;

procedure TDcmp.SaveToFolder(p: String);
var
  entry: TDcmpFileEntry;
  I: Integer;
  outFile: TFileStream;
begin
  for entry in fileEntries do
  begin
    outFile := TFileStream.Create(p + entry.name, fmCreate);
    outFile.CopyFrom(entry.fileStream, 0);
    outFile.Free;
  end;
end;

procedure TDcmp.FolderToDcmp(f: String);
var
  FolderFiles: TStringList;
  currFile: TDcmpFileEntry;
  FileName: String;
  I: Integer;
  outMemoryStream: TMemoryStream;
  indexSize: Integer;
  runningOffset: Integer;
  outIndexStream: TMemoryStream;
  outFilesStream: TMemoryStream;
  outDcmpFileMemStream: TCustomMemoryStream;
  outDcmpFileStream: TFileStream;
  nullByteVar: Byte = $00;
begin
  FolderFiles := TStringList.Create;
  FindAllFiles(FolderFiles, f);
  SetLength(fileEntries, FolderFiles.Count);

  indexSize := 64 * FolderFiles.Count;
  runningOffset := indexSize;

  outIndexStream := TMemoryStream.Create;
  outFilesStream := TMemoryStream.Create;

  for I := 0 to FolderFiles.Count - 1 do
  begin
    // Set our dummies to 0
    fileEntries[I].dummy1 := 0;
    fileEntries[I].dummy2 := 0;

    // Set our offset to our running offset tracking the resulting output location
    fileEntries[I].offset := runningOffset;

    // Set the ID and name. Assuming ID is always based on alphabetical order.
    fileEntries[I].id := I;
    fileEntries[I].name := ExtractFileName(FolderFiles[I]);

    // Read the file contents into memory
    fileEntries[I].fileStream := TMemoryStream.Create;
    fileEntries[I].fileStream.LoadFromFile(f + '/' + fileEntries[I].name);

    // Set the size and then increment our running offset
    fileEntries[I].size := fileEntries[I].fileStream.Size;
    Inc(runningOffset, fileEntries[I].size);

    // Add this file's contents to our running stream
    outFilesStream.CopyFrom(fileEntries[I].fileStream, 0);

    // Build the index entry
    with outIndexStream do
    begin
      if I = 0 then
        WriteWord(FolderFiles.Count)
      else
        WriteWord(fileEntries[I].dummy1);

      WriteWord(fileEntries[I].dummy2);
      WriteDWord(fileEntries[I].offset);
      WriteDWord(fileEntries[I].size);
      WriteDWord(fileEntries[I].id);
      Write(fileEntries[I].name[1], 48);

      // Pad out the header if the name didn't take up enough space
      while (Size mod 64) > 0 do
        Write(nullByteVar, 1);

    end;
  end;

  // Finally Copy to outDcmpFile
  outDcmpFileMemStream := TMemoryStream.Create;
  outDcmpFileMemStream.CopyFrom(outIndexStream, 0);
  outDcmpFileMemStream.CopyFrom(outFilesStream, 0);

  outDcmpFileMemStream.SaveToFile(f + '.dmcp');
  outDcmpFileMemStream.Free;

  FolderFiles.Free;

end;

{ TLzs }
constructor TLzs.Create;
begin

end;

procedure TLzs.LoadFromLzFile(p: String);
begin
  // if input file is lzs
  lzStream := TMemoryStream.Create;
  lzStream.LoadFromFile(p);   
  GetLzTypeAndSize;
end;


procedure TLzs.CompressFile(p: String; lz_type: Integer = 0);
begin
  decStream := TMemoryStream.Create;
  decStream.LoadFromFile(p);

  lzStream := TMemoryStream.Create;
                        
  decLength := decStream.Size;

  lzType := lz_type;

  if lzType = 0 then
    EncodeLzss;

  SaveLzStreamToFile(p + '.lzs');

end;

procedure TLzs.GetLzTypeAndSize;
var
  firstWord: Cardinal;
begin
  firstWord := lzStream.ReadDWord();
  lzType := (firstWord >> 30);
  decLength := (firstWord and $3FFFFFFF);
end;

procedure TLzs.WriteLzFirstWord;
var
  firstWord: Cardinal;
begin
  firstWord := (lzType << 30);
  firstWord := (firstWord or decLength);

  lzStream.Position := 0;
  lzStream.WriteDWord(firstWord);
end;

procedure TLzs.DecodeLzStream;
begin
  decStream := TMemoryStream.Create;

  if lzType = 0 then
    DecodeLzss;
  if lzType = 3 then
    DecodeLz4;
end;

function GetIndexOfSubarrayIfExists(search_for, search_in: Array of byte): Integer;
var               
  currByte: Byte;
  I: Integer = 0;
  matched_bytes: Integer = 0;
begin

  for currByte in search_in do
  begin
    if search_for[matched_bytes] = currByte then
       Inc(matched_bytes)
    else
        matched_bytes := 0;
          
    Inc(I);

    if Length(search_for) <= matched_bytes then
       Break;
  end;

  if matched_bytes < Length(search_for) then
    GetIndexOfSubarrayIfExists := -1
  else
    GetIndexOfSubarrayIfExists := I - Length(search_for);

end;

procedure TLzs.EncodeLzss;
var
  search_buffer: array of Byte;  // The lookback window of bytes
  check_characters: array of Byte;  // The current byte pattern to look for in search_buffer
  currByte: Byte;
  writeByte: Byte;
  index: Integer;
  I: Integer = 0;
  offset: Word;
  offsetToWrite: array[0..1] of Byte;
  tokenPos: Word;
  tokenByte: Byte = $0;
  tokenCounter: Integer = 0;
begin

  LzStream.Position := 0;
  WriteLzFirstWord;

  tokenPos := LzStream.Position;
  // DebugLn('Writing token byte at position ' + IntToStr(LzStream.Position));
  LzStream.WriteByte(tokenByte);

  decStream.Position := 0;
  while decStream.Position < decLength - 1 do
  //while decStream.Position < 10240 do
  begin                                        
    //DebugLn('lzStream pos ' + IntToStr(lzStream.Position));
    //lzStream.WriteByte($FF);
    //
    //lzStream.CopyFrom(decStream, 8);              
    //DebugLn('decStream pos: ' + IntToStr(decStream.Position));


    if Length(check_characters) = -1 then // Unset length, set to 1
      SetLength(check_characters, 1)
    else // Make room for the next byte
      SetLength(check_characters, Length(check_characters) + 1);

    // read the current byte
    currByte := decStream.ReadByte;

    // add the newly read byte to the top-most index
    check_characters[High(check_characters)] := currByte;

    index := GetIndexOfSubarrayIfExists(check_characters, search_buffer);

    if (index = -1) or (I = decLength - 1) or (Length(check_characters) = 18) then
      // check_characters wasn't found in search_buffer
      begin
        if Length(check_characters) > 3 then
          // The previous round of checking had a match! let's go back to that simpler time
          // > 2 above to avoid writing a token that's too small
          begin
            Delete(check_characters, High(check_characters), 1);
            index := GetIndexOfSubarrayIfExists(check_characters, search_buffer);

            // Calculate the offset
            offset := I - index - Length(check_characters);

            // The format of the offset to write to the LZ Stream is
            // Two bytes split into four nibbles
            // [0]  [1]  [2]  [3]
            // 1111 1111 1111 1111       
            // Nibbles 2, 0, 1 store the offset, again after a bit of a wild calculation

            // from the decoder
            //offset := (instr[1] >> 4 << 8 or instr[0]) + 18;
            //actOffset := ((offset + readIter) - (decStream.Size mod LZSS_LOOKBACK_WINDOW) + LZSS_LOOKBACK_WINDOW);
            //actOffset := (actOffset mod LZSS_LOOKBACK_WINDOW) + decStream.Size - LZSS_LOOKBACK_WINDOW;
            // what a hassle right
            offset := offset + LZSS_LOOKBACK_WINDOW - 18;

            // Nibble [3] is the length to copy from the lookback window, minus 3
            // Why minus 3? I suppose because 3 is the minimum size. By subtracting
            // 3 before storing it, this value can actually store a value up to 18
            // instead of only limiting to 15
            offsetToWrite[1] := Length(check_characters) - 3;

            // some other shifting

            offsetToWrite[0] := offset and $ff;
            offsetToWrite[1] := offsetToWrite[1] or ((offset >> 4) and $f0);

            lzStream.WriteBuffer(offsetToWrite, 2);

            // Increment the token counter. We don't make a change to the flag since 0 means token
            Inc(tokenCounter);         
            if tokenCounter > 7 then
            begin
              tokenPos := lzStream.Position;
              tokenByte := $0;
              tokenCounter := 0;
              lzStream.WriteByte(tokenByte);
            end;
          end
        else
        begin
          for writeByte in check_characters do
          begin
            // DebugLn('Writing ' + IntToHex(writeByte) + ' to pos ' + IntToStr(lzStream.Position));
            lzStream.WriteByte(writeByte);
            // DebugLn('lzStream pos ' + IntToStr(lzStream.Position));

            tokenByte := tokenByte or (1 << tokenCounter);
            lzStream.Position := tokenPos;
            lzStream.WriteByte(tokenByte);
            Inc(tokenCounter);

            // Move to end of LzStream
            lzStream.Position := lzStream.Size;
            if tokenCounter > 7 then
            begin
              tokenPos := lzStream.Position;
              tokenByte := $0;
              tokenCounter := 0;
              lzStream.WriteByte(tokenByte);
            end;

          end;
        end;

        SetLength(check_characters, 0);
      end;           


    if Length(search_buffer) = -1 then // Unset length, set to 1
      SetLength(search_buffer, 1)
    else // Make room for the next byte
      SetLength(search_buffer, Length(search_buffer) + 1);

    search_buffer[High(search_buffer)] := currByte;

    if Length(search_buffer) = LZSS_LOOKBACK_WINDOW then
      Delete(search_buffer, 0, 1);

    Inc(I);
  end;
end;

procedure TLzs.DecodeLzss;
var
  currentBit: Integer;
  flagByte: Byte;
  instr: array[0..1] of Byte;
  offset: Integer;
  actOffset: Integer;
  readLength: Integer;
  readIter: Integer;
  byteToWrite: Byte;
begin
  DebugLn('Begin routine for lzType lzss0');

  while (decStream.Size < decLength) do
  begin
    if decStream.Size >= decLength then
      Break;

    flagByte := lzStream.ReadByte();

    for currentBit := 0 to 7 do
    begin
      if decStream.Size >= decLength then
        Break;

      if ((flagByte shr currentBit) and 1) = 1 then
        begin
          byteToWrite := lzStream.ReadByte();
          decStream.WriteByte(byteToWrite);
        end
      else
        begin                 
          //DebugLn('At file position ' + IntToStr(lzStream.Position));
          lzStream.Read(instr, 2);
          // instr[0] = $E8; 1110 1000
          // instr[1] = $F3; 1111 0011

          // instr[1] >> 4:  0000 1111
          // instr[1] << 8:  1111 0000 0000
          // or instr[0]     0000 1110 1000
          // result          1111 1110 1000
          offset := (instr[1] >> 4 << 8 or instr[0]) + 18;
          //DebugLn('Offset: ' + IntToStr(offset));


          // instr[1] = $F3; 1111 0011
          // and $0f;        0000 1111
          // result:         0000 0011
          readLength := (instr[1] and $0f) + 3;
                                               
          //DebugLn('Read length: ' + IntToStr(readLength));
          for readIter := 0 to readLength - 1 do
            begin
              actOffset := ((offset + readIter) - (decStream.Size mod LZSS_LOOKBACK_WINDOW) + LZSS_LOOKBACK_WINDOW);
              actOffset := (actOffset mod LZSS_LOOKBACK_WINDOW) + decStream.Size - LZSS_LOOKBACK_WINDOW;     
              //DebugLn('Actual Offset: ' + IntToStr(actOffset));

              if actOffSet < 0 then
                begin
                  byteToWrite := 0;
                end
              else
                begin
                  decStream.Position := actOffset;
                  byteToWrite := decStream.ReadByte();
                  decStream.Position := decStream.Size;
                end;
              decStream.WriteByte(byteToWrite);
            end;
        end;
    end;
  end;
end;

procedure TLzs.DecodeLz4;
var
  readIter: Integer;
  lz4TokenByte: Byte;
  lz4LiteralsToRead: Cardinal;
  lz4DecodedBytesToCopy: Cardinal;
  lz4LookbackOffset: Cardinal;
  bytesToWrite: array of Byte;
begin
  DebugLn('Begin routine for lzType lz4');

  while (decStream.Size < decLength) do
  begin
    lz4TokenByte := lzStream.ReadByte();

    lz4LiteralsToRead := lz4TokenByte >> 4;
    lz4DecodedBytesToCopy := lz4TokenByte and $0f;

    if lz4LiteralsToRead = $0f then
      Extend(lz4LiteralsToRead);

    SetLength(bytesToWrite, lz4LiteralsToRead);

    if lz4LiteralsToRead > 0 then
      begin
        lzStream.Read(bytesToWrite[0], lz4LiteralsToRead);
        decStream.Write(bytesToWrite[0], lz4LiteralsToRead);
      end;

    SetLength(bytesToWrite, 0);

    if decStream.Size >= decLength then break;

    lz4LookbackOffset := lzStream.ReadWord();

    if lz4DecodedBytesToCopy = $0f then
      Extend(lz4DecodedBytesToCopy);

    SetLength(bytesToWrite, (lz4DecodedBytesToCopy + 4));
    decStream.Position := decStream.Position - lz4LookbackOffset;
    decStream.Read(bytesToWrite[0], Length(bytesToWrite));

    for readIter := 0 to Length(bytesToWrite) - 1 do
      begin
        if (lz4LookbackOffset = 1) and (bytesToWrite[readIter] = $0) then
          bytesToWrite[readIter] :=  bytesToWrite[0];
      end;

    decStream.Position := decStream.Size;
    decStream.Write(bytesToWrite[0], Length(bytesToWrite));
  end;
end;

procedure TLzs.SaveLzStreamToFile(p: String);
var
  outFileStream: TFileStream;
begin
  outFileStream := TFileStream.Create(p, fmCreate);
  outFileStream.CopyFrom(lzStream, 0);
  outFileStream.Free;
end;

procedure TLzs.SaveDecStreamToFile(p: String);
var
  outFileStream: TFileStream;
begin
  outFileStream := TFileStream.Create(p, fmCreate);
  outFileStream.CopyFrom(decStream, 0);
  outFileStream.Free;
end;

procedure TLzs.Extend(var int: longWord);
var
  currByte: Byte;
begin
  currByte := lzStream.ReadByte();
  int := int + currByte;
  if currByte = $ff then
    Extend(int);
end;

end.

