program sosawlmessageeditor;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  {$IFDEF HASAMIGA}
  athreads,
  {$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms,
  main,
  MessageEditorForm,
  filetools,
  lzsTools,
  sosNpcScheduleFile,
  sosNpcScheduleEditorForm, awlMessage, awlscripts;

{$R *.res}

begin
  RequireDerivedFormResource:=True;
  Application.Scaled:=True;
  Application.Initialize;
  Application.CreateForm(TLauncherForm, LauncherForm);
  Application.CreateForm(TFileToolsForm, FileToolsForm);
  Application.CreateForm(TLzsToolsForm, LzsToolsForm);
  Application.CreateForm(TNPCScheduleEditorForm, NPCScheduleEditorForm);
  Application.Run;
end.

