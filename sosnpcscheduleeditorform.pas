unit sosNpcScheduleEditorForm;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, Grids, StdCtrls, Menus,
  ExtCtrls, sosNpcScheduleFile, LazUTF8, LazLogger;

type

  { TNPCScheduleEditorForm }

  TNPCScheduleEditorForm = class(TForm)
    ActivityComboLabel: TLabel;
    ApplyButton: TButton;
    ChapterCheckGroup: TCheckGroup;
    MenuItem1: TMenuItem;
    SaveDialog1: TSaveDialog;
    ScheduleFlagsCheckGroup: TCheckGroup;
    SaveCSVDialog: TSaveDialog;
    UtilitySaveToCSVMenu: TMenuItem;
    UtilitiesMenu: TMenuItem;
    ScheduleComboLabel: TLabel;
    ScheduleActivityListComboBox: TComboBox;
    MainMenu1: TMainMenu;
    FileMenu: TMenuItem;
    FileMenuOpen: TMenuItem;
    OpenDialog1: TOpenDialog;
    ScheduleComboBox: TComboBox;
    ScheduleActivityGrid: TStringGrid;
    procedure ApplyButtonClick(Sender: TObject);
    procedure FileMenuOpenClick(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure OpenFile(p: String);
    procedure PopulateScheduleCombo;
    procedure ScheduleActivityListComboBoxChange(Sender: TObject);
    procedure ScheduleComboBoxChange(Sender: TObject);
    procedure PopulateScheduleActivityListCombo;
    procedure PopulateGrid;
    procedure ApplyGridValuesToNPCSchedule;
    procedure UtilitySaveToCSVMenuClick(Sender: TObject);
  private

  public

  end;

var
  NPCScheduleEditorForm: TNPCScheduleEditorForm;
  NPCScheduleFile: TNPCScheduleFile;
  NPCSchedule: TNPCSchedule;

implementation

{$R *.lfm}

{ TNPCScheduleEditorForm }

procedure TNPCScheduleEditorForm.FileMenuOpenClick(Sender: TObject);
begin
  if OpenDialog1.Execute then
  begin
    OpenFile(UTF8ToSys(OpenDialog1.FileName));
    PopulateScheduleCombo;
  end;
end;

procedure TNPCScheduleEditorForm.ApplyButtonClick(Sender: TObject);
begin
  ApplyGridValuesToNPCSchedule;
end;

procedure TNPCScheduleEditorForm.MenuItem1Click(Sender: TObject);
begin
  if SaveDialog1.Execute then
  begin
    NPCScheduleFile.SaveToFile(SaveDialog1.FileName);
  end;
end;


procedure TNPCScheduleEditorForm.OpenFile(p: String);
begin
  NPCScheduleFile := TNPCScheduleFile.Create;
  NPCScheduleFile.LoadFromFile(p);
end;

procedure TNPCScheduleEditorForm.PopulateScheduleCombo;
var
  I: Integer;
begin
  ScheduleComboBox.Items.Clear;
  for I := 0 to NPCScheduleFile.file_size - 1 do
  begin
    ScheduleComboBox.Items.Add(IntToStr(I) + ' [' + IntToStr(NPCScheduleFile.Schedules[I].schedule_size )+ ']');
  end;
end;

procedure TNPCScheduleEditorForm.ScheduleActivityListComboBoxChange(
  Sender: TObject);
begin
  PopulateGrid;
end;

procedure TNPCScheduleEditorForm.ScheduleComboBoxChange(Sender: TObject);
begin
  PopulateScheduleActivityListCombo;
end;

procedure TNPCScheduleEditorForm.PopulateScheduleActivityListCombo;
var
  currSched: TNPCSchedule;
  I: Integer;
begin
  currSched := NPCScheduleFile.Schedules[ScheduleComboBox.ItemIndex];
  ScheduleActivityListComboBox.Items.Clear;
  for I := 0 to currSched.schedule_size - 1 do
  begin
    ScheduleActivityListComboBox.Items.Add(IntToStr(I));
  end;
end;

procedure TNPCScheduleEditorForm.PopulateGrid;
var
  currSched: TNPCSchedule;
  currSchedList: TNPCScheduleActivityList;
  currActivity: TNPCScheduleActivity;
  I, J: Integer;
begin
  currSched := NPCScheduleFile.Schedules[ScheduleComboBox.ItemIndex];
  currSchedList := currSched.ScheduleLists[ScheduleActivityListComboBox.ItemIndex];

  ScheduleActivityGrid.RowCount := 1;
  ScheduleActivityGrid.RowCount := currSchedList.list_size + 1;
  for I := 0 to currSchedList.list_size - 1 do
  begin
    ScheduleActivityGrid.Cells[0, I + 1] := IntToStr(currSchedList.ScheduleActivities[I].start_minute);
    ScheduleActivityGrid.Cells[1, I + 1] := IntToStr(currSchedList.ScheduleActivities[I].end_minute);
    ScheduleActivityGrid.Cells[2, I + 1] := IntToStr(currSchedList.ScheduleActivities[I].task_id);

    for J := 0 to 7 do
    begin
      ChapterCheckGroup.Checked[J] := currSchedList.YearFlags[J];
    end;

    for J := 0 to 23 do
    begin
      ScheduleFlagsCheckGroup.Checked[J] := currSchedList.SchedFlags[J];
    end;
  end;
end;

procedure TNPCScheduleEditorForm.ApplyGridValuesToNPCSchedule;
var
  I, J: Integer;
  tmpScheduleIndex: Integer;
  tmpActivityListIndex: Integer;
  tmpChapterFlags: LongWord = 0;
  tmpSchedFlags: LongWord = 0;
  currBitFlag: Integer;
  act_start: LongWord;
  act_end: LongWord;
  task_id: LongWord;
begin
  tmpScheduleIndex := ScheduleComboBox.ItemIndex;
  tmpActivityListIndex := ScheduleActivityListComboBox.ItemIndex;

  // Update Chapter Flags
  for I := 0 to 7 do
  begin
    currBitFlag := 0;
    if ChapterCheckGroup.Checked[I] then
       currBitFlag := 1;

    currBitFlag := currBitFlag shl I;
    tmpChapterFlags := tmpChapterFlags or currBitFlag;
  end;

  NPCScheduleFile
            .Schedules[tmpScheduleIndex]
            .ScheduleLists[tmpActivityListIndex]
            .SetYearFlags(tmpChapterFlags);

  // Update Schedule Flags    
  for I := 0 to 23 do
  begin             
    currBitFlag := 0;
    if ScheduleFlagsCheckGroup.Checked[I] then
       currBitFlag := 1;
                            
    currBitFlag := currBitFlag shl I;
    tmpSchedFlags := tmpSchedFlags or currBitFlag;
  end;

  NPCScheduleFile
            .Schedules[tmpScheduleIndex]
            .ScheduleLists[tmpActivityListIndex]
            .SetSchedFlags(tmpSchedFlags);

  // Update Activity List
  NPCScheduleFile
            .Schedules[tmpScheduleIndex]
            .ScheduleLists[tmpActivityListIndex]
            .ResetActivities;

  for I := 1 to ScheduleActivityGrid.RowCount - 1 do
  begin
    act_start := StrToInt(ScheduleActivityGrid.Cells[0, I]);
    act_end   := StrToInt(ScheduleActivityGrid.Cells[1, I]);
    task_id   := StrToInt(ScheduleActivityGrid.Cells[2, I]);

    NPCScheduleFile
            .Schedules[tmpScheduleIndex]
            .ScheduleLists[tmpActivityListIndex]
            .AddActivity(act_start, act_end, task_id);
  end;
end;

procedure TNPCScheduleEditorForm.UtilitySaveToCSVMenuClick(Sender: TObject);
begin
  if SaveCSVDialog.Execute then
  begin
    ScheduleActivityGrid.SaveToCSVFile(SaveCSVDialog.FileName);
  end;
end;

end.

