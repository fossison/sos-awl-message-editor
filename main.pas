unit main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls,
  MessageEditorForm,
  filetools,
  sosNpcScheduleEditorForm;

type

  { TLauncherForm }

  TLauncherForm = class(TForm)
    SchedEditorButton: TButton;
    MsgEditorLabel: TLabel;
    MsgEditorButton: TButton;
    FileToolsButton: TButton;
    MsgEditorLabel1: TLabel;
    procedure FileToolsButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure MsgEditorButtonClick(Sender: TObject);
    procedure SchedEditorButtonClick(Sender: TObject);
  private

  public

  end;

var
  LauncherForm: TLauncherForm;

implementation

{$R *.lfm}

procedure TLauncherForm.FormCreate(Sender: TObject);
begin

end;

procedure TLauncherForm.FileToolsButtonClick(Sender: TObject);
begin
  with TFileToolsForm.Create(nil) do
    Show;
end;

procedure TLauncherForm.MsgEditorButtonClick(Sender: TObject);
begin
  with TMessageEditorForm.Create(nil) do
    Show;
end;

procedure TLauncherForm.SchedEditorButtonClick(Sender: TObject);
begin
  with TNPCScheduleEditorForm.Create(nil) do
    Show;
end;


end.

