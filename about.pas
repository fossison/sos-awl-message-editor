unit About;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls;

type

  { TAboutForm }

  TAboutForm = class(TForm)
    Image1: TImage;
    Label1: TLabel;
    ProgName: TLabel;
    Shape1: TShape;
    procedure FormCreate(Sender: TObject);
    procedure Label1Click(Sender: TObject);
    procedure ProgNameClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end; 

var
  AboutForm: TAboutForm;

implementation

{$R *.lfm}

{ TAboutForm }

procedure TAboutForm.FormCreate(Sender: TObject);
begin

end;

procedure TAboutForm.Label1Click(Sender: TObject);
begin

end;

procedure TAboutForm.ProgNameClick(Sender: TObject);
begin

end;


end.

