unit HMutils;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, awlmessage, Forms, LazLogger;

procedure exportMesToTxt(unkOnly: boolean = false; mes: TMes = nil);

implementation

procedure exportMesToTxt(unkOnly: boolean = false; mes: TMes = nil);
var
  msg: TMsg;
  exportFilePath: string = '';
begin
  if mes.Path <> '' then
    begin
      if unkOnly = true then
        begin
        exportFilePath := ExtractFileName(mes.Path) + '.unk.txt';
        end
      else
        exportFilePath := ExtractFileName(mes.Path) + '.txt';

      with TStringList.Create do
      try
        if unkOnly = true then
          Add('Exported only messages with unknown bytes.' + sLineBreak + sLineBreak);
        for msg in Mes.MsgCollection do
        begin
          //if (unkOnly = false) or (unkOnly = true and Msg.mBytesUnknown = true) then
            Add('Message ' + IntToStr(msg.Index) + ':' + sLineBreak + msg.MText + sLineBreak);
        end;
        SaveToFile(exportFilePath);
      finally
        Free;
        Application.MessageBox('Successfully exported!', 'Success', 1);
        DebugLn('Exported to ' + exportFilePath);
      end
    end
  else
    Application.MessageBox('No file is currently opened', 'Error', $00000030);
end;

end.

