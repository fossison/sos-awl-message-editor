unit soshelpers;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, dutf8, dutf16;

function ConvertUTF8ToUTF16(const S: AnsiString; BigEndian: Boolean = True): WideString;

type
  TUTF16Bytes = array of PWideChar;

implementation

function ConvertUTF8ToUTF16(const S: AnsiString; BigEndian: Boolean = True): WideString;
var
  L: SizeUInt;
  U: Cardinal;
  SCursor, SCursorEnd: PAnsiChar;
  WCursor, WCursorEnd: PWideChar;
begin
  // In the first pass determine the size of resulting string.
  L := 0;
  SCursor := @S[1];
  SCursorEnd := SCursor + Length(S);
  while DecodeUTF8CharReplace(SCursor, SCursorEnd, U, Ord('?')) do
    Inc(L, GetUTF16CharSize(U));
  // GetUTF16CharSize returns number of bytes.
  // But we want to compute number of WideChars. SizeOf(WideChar) = 2.
  L := L div 2;
  // In the second pass encode resulting UTF-16 string.
  SetLength(Result, L);
  SCursor := @S[1];
  SCursorEnd := SCursor + Length(S);
  WCursor := @Result[1];
  WCursorEnd := WCursor + Length(Result);
  while DecodeUTF8CharReplace(SCursor, SCursorEnd, U, Ord('?')) do
    EncodeUTF16Char(U, WCursor, WCursorEnd, BigEndian);
end;

end.

